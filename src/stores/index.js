import { defineStore } from 'pinia'

export const useIndexStore = defineStore('index', {
    state: () => {
        return {
            ordinalsBtcObj: {
                balance: 0,
                balanceUsd: 0
            },
            ordinalsTokenList: [],
            mappingOrdinalsToken: [],
            aaAddress: {
                signature: ''
            },
            displayWalletRecharge: false
        }
    },
    actions: {}
})
