import { defineStore } from 'pinia'

export const usePersistStore = defineStore('persist', {
    state: () => {
        return {
            walletInfo: {
                walletAddres: '', // 钱包地址
                showWalletAddres: '' // 展示地址
            },
            userInfo: {
                token: '',
                userId: ''
            },
            canBeClaimed: 0
        }
    },
    actions: {
        walletLogoutLogin() {
            this.walletInfo = {
                walletAddres: '', // 钱包地址
                showWalletAddres: '', // 展示地址
                signature: '' // 签名回调秘钥
            }
            this.userInfo = {
                token: '',
                userId: ''
            }
        },
        setWalletAddresAndShow(address) {
            this.walletInfo.walletAddres = address
            this.walletInfo.showWalletAddres = `${address.slice(0, 5)}...${address.slice(
                address.length - 5,
                address.length
            )}`
        }
    },
    persist: true
})
