import { createRouter, createWebHistory } from 'vue-router'
import stats from '@/views/stats.vue'
import claim from '@/views/claim.vue'
import airdrop from '@/views/airdrop.vue'

const router = createRouter({
    history: createWebHistory(),
    routes: [
        { path: '/', redirect: '/airdrop' },
        {
            path: '/stats',
            name: 'stats',
            component: stats
        },
        {
            path: '/airdrop',
            name: 'airdrop',
            component: airdrop
        },
        {
            path: '/claim',
            name: 'claim',
            component: claim
        }
    ]
})

export default router
