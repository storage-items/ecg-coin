import { ElNotification } from 'element-plus'
import { usePersistStore } from '@/stores/persist.js'
import { userDoLong, twitterOauth1RequestToken } from '@/assets/api/index.js'

export const switchAddChain = async () => {
    await window.ethereum.request({
        method: 'wallet_switchEthereumChain',
        params: [{ chainId: window.web3.utils.toHex(1) }]
    })
}
export const establishAConnection = async () => {
    // 判断是否链接并建立连接返回钱包地址
    try {
        let address = await ethereum.request({ method: 'eth_requestAccounts' }) //授权连接钱包
        return address[0]
    } catch (error) {
        ElNotification({
            grouping: true,
            message: "You don't Have a MetaMask Wallet yet.",
            type: 'error'
        })
        return false
    }
}
export const metaMaskAccountsChanged = async () => {
    // 监听切链操作
    if (window.ethereum) {
        const persistStore = usePersistStore()
        window.ethereum.on('chainChanged', function (chainId) {
            persistStore.walletLogoutLogin()
        })
        window.ethereum.on('accountsChanged', function (accounts) {
            console.log('adsasdasdaaa')
            persistStore.walletLogoutLogin()
        })
    }
}
export const metaMaskVerification = async () => {
    // 请求钱包签名
    let address = await establishAConnection()
    if (!address) return false
    let signature
    const message = `Welcome to the ecgcoin Questboard, please sign this message to verify your identity.`
    try {
        signature = await window.ethereum.request({
            method: 'personal_sign',
            params: [address, message]
        })
        const persistStore = usePersistStore()
        persistStore.setWalletAddresAndShow(address)
        persistStore.walletInfo.signature = signature
        const onUserDoLongRes = await onUserDoLong({
            address,
            message,
            signature
        })
        return onUserDoLongRes
    } catch (error) {
        ElNotification({
            message: 'User rejected request',
            type: 'error'
        })
        return false
    }
}
export const onUserDoLong = async (parameter) => {
    const userDoLongRef = await userDoLong(parameter)
    if (userDoLongRef.code == 200) {
        ElNotification({
            title: 'Successfully Connect wallet',
            type: 'success'
        })
        return true
    } else {
        ElNotification({
            title: userDoLongRef.message,
            type: 'error'
        })
        return false
    }
}
export const twitterAuthorization = async () => {
    // 跳转推特认证
    const res = await twitterOauth1RequestToken({
        callbackUrl: encodeURIComponent(`${import.meta.env.VITE_APP_PATH}/airdrop`)
    })
    if (res && res.data) {
        window.location.href = res.data
        return true
    } else {
        ElNotification({
            title: 'Twitter authentication failed',
            message: res.desc,
            type: 'error'
        })
    }
}
