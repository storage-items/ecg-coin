// import BigNumber from 'bignumber.js'

// 结构请求参数
export const objToUrl = (obj) => {
    const tempArray = []
    for (const item in obj) {
        if (item) {
            tempArray.push(`${item}=${obj[item]}`)
        }
    }
    return `?${tempArray.join('&')}`
}
// 获取某一个参数值
export const getQueryString = (name) => {
    try {
        const params = new URLSearchParams(window.location.search)
        return params.get(name)
    } catch (error) {
        return null
    }
}
// 复制方法
export const copyTextToClipboard = (text, cb) => {
    navigator.clipboard.writeText(text).then(() => {
        if (cb) cb()
    })
}
// 生成 500,000
export const addCommasToNumber = (number) => {
    if (typeof number !== 'number' && typeof number !== 'string') {
        return number // 返回原值，如果输入无效
    }
    // 调用 toLocaleString 方法，设置地区为 'en-US' 以使用逗号作为分隔符
    return Number(number).toLocaleString('en-US')
}
// 防抖函数
export const debounce = (func, delay) => {
    let timer
    return function (...args) {
        clearTimeout(timer)
        timer = setTimeout(() => {
            func.apply(this, args)
        }, delay)
    }
}
// 节流函数
export function throttle(func, delay) {
    let lastCall = 0
    return function (...args) {
        const now = new Date().getTime()
        if (now - lastCall < delay) return
        lastCall = now
        return func.apply(this, args)
    }
}
// 保留数字的指定位数小数
export function formatNumber(number, decimalPlaces) {
    if (isNaN(number)) {
        return '0'
    }
    const thisNum = Number(number)
    const roundedNumber = thisNum.toFixed(decimalPlaces)
    return parseFloat(roundedNumber).toString()
}
// 转换时间格式为 31-08-2023 10:07:23
export function formatDate(datess) {
    let date = new Date(datess)
    const day = String(date.getDate()).padStart(2, '0')
    const month = String(date.getMonth() + 1).padStart(2, '0') // 月份是从0开始的，所以我们需要+1
    const year = date.getFullYear()

    const hours = String(date.getHours()).padStart(2, '0')
    const minutes = String(date.getMinutes()).padStart(2, '0')
    const seconds = String(date.getSeconds()).padStart(2, '0')

    return `${day}-${month}-${year} ${hours}:${minutes}:${seconds}`
}
// // 缩小18位小数
// export function shiftDecimalPointMin(number) {
//     const bigNumber = new BigNumber(number)
//     return bigNumber.dividedBy(new BigNumber(10).pow(18)).toFixed()
// }
// // 放大18位小数
// export function shiftDecimalPointMax(number) {
//     const bigNumber = new BigNumber(number)
//     return bigNumber.multipliedBy(new BigNumber(10).pow(18)).toFixed()
// }
// // 两数相乘
// export function multiply(a, b, decimalPlaces = 4) {
//     const result = new BigNumber(a).times(b)
//     return result.toFixed(decimalPlaces).toString()
// }

// // 两数相除
// export function divide(a, b, decimalPlaces = 4) {
//     const result = new BigNumber(a).div(b)
//     return result.toFixed(decimalPlaces).toString()
// }
// // 两数相加
// export function add(a, b, decimalPlaces = 4) {
//     const result = new BigNumber(a).plus(b)
//     return result.toFixed(decimalPlaces).toString()
// }

// // 两数相减
// export function subtract(a, b, decimalPlaces = 4) {
//     const result = new BigNumber(a).minus(b)
//     return result.toFixed(decimalPlaces).toString()
// }
