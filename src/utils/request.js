import axios from 'axios'
import { usePersistStore } from '@/stores/persist.js'
import { getQueryString } from '@/utils/utils.js'
import { ElNotification } from 'element-plus'
let service = axios.create({
    baseURL: '',
    timeout: 20000
})
// 请求拦截器
service.interceptors.request.use(
    (config) => {
        const persistStore = usePersistStore()
        config.headers['AuthAuthorize'] = persistStore.userInfo?.token || ''
        config.url = import.meta.env.DEV ? config.url : import.meta.env.VITE_API_PATH + config.url
        return config
    },
    (error) => {
        Promise.reject(error)
    }
)
//响应拦截器
service.interceptors.response.use(
    async (response) => {
        return response.data
    },
    (error) => {
        return Promise.reject(error)
    }
)

export default service
