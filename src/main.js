import { createApp } from 'vue'
import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import ElementPlus from 'element-plus'
import '@/assets/scss/tailwind.scss'
import 'element-plus/dist/index.css'
import '@/assets/scss/main.scss'
import App from '@/App.vue'
import router from '@/router'

const pinia = createPinia()
pinia.use(piniaPluginPersistedstate)
const app = createApp(App)

app.use(router)
app.use(pinia)
app.use(ElementPlus)
app.mount('#app')
