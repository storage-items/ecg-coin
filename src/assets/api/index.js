import service from '@/utils/request.js'
import { objToUrl } from '@/utils/utils.js'

// 登录请求
export const userDoLong = (data) => {
    return service({
        url: `/api/user/doLogin`,
        method: 'post',
        data
    })
}
// 授权Step1
export const twitterOauth1RequestToken = (data) => {
    return service({
        url: `/api/twitter/auth/get/authorizationUrl${objToUrl(data)}`,
        method: 'get'
    })
}
// 授权Step3
export const twitterOauth1AccessToken = (data) => {
    return service({
        url: `/api/twitter/auth/get/accessToken${objToUrl(data)}`,
        method: 'get'
    })
}
// 校验持币比例积分
export const integralCheckBalanceAward = (data) => {
    return service({
        url: `/api/integral/check/balance/award${objToUrl(data)}`,
        method: 'get'
    })
}
// 领取持币比例积分
export const integralGetBalanceAward = (data) => {
    return service({
        url: `/api/integral/get/balance/award${objToUrl(data)}`,
        method: 'get'
    })
}
// 查询用户积分
export const integralGetInfo = (data) => {
    return service({
        url: `/api/integral/get/info`,
        method: 'get'
    })
}
// 积分排行榜
export const integralGetLog = (data) => {
    return service({
        url: `/api/integral/get/log`,
        method: 'get'
    })
}
